package main

import "github.com/veandco/go-sdl2/sdl"

var (
	// KeyMap of modern keyboard keys to CHIP-8 keys.
	KeyMap = map[sdl.Scancode]uint{
		sdl.SCANCODE_X: 0x0,
		sdl.SCANCODE_1: 0x1,
		sdl.SCANCODE_2: 0x2,
		sdl.SCANCODE_3: 0x3,
		sdl.SCANCODE_Q: 0x4,
		sdl.SCANCODE_W: 0x5,
		sdl.SCANCODE_E: 0x6,
		sdl.SCANCODE_A: 0x7,
		sdl.SCANCODE_S: 0x8,
		sdl.SCANCODE_D: 0x9,
		sdl.SCANCODE_Z: 0xA,
		sdl.SCANCODE_C: 0xB,
		sdl.SCANCODE_4: 0xC,
		sdl.SCANCODE_R: 0xD,
		sdl.SCANCODE_F: 0xE,
		sdl.SCANCODE_V: 0xF,
	}
)

func Release_key(key uint) {
	if key < 16 {
		VM.Keyboard[key] = false
	}
}

func Press_key(key uint) {
	VM.Keyboard[key] = true
}
