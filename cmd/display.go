package main

import (
	"log"

	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/stickman_0x00/go_chip8/chip8"
)

var (
	Window   *sdl.Window
	Renderer *sdl.Renderer
	Screen   *sdl.Texture

	ratio  = 10
	width  = int32(chip8.WIDTH * ratio)
	height = int32(chip8.HEIGHT * ratio)
)

// createWindow creates the SDL window and renderer or panics.
func create_window() {
	var err error

	// window attributes
	flags := sdl.WINDOW_OPENGL

	// create the window and renderer
	Window, Renderer, err = sdl.CreateWindowAndRenderer(width, height, uint32(flags))
	if err != nil {
		log.Fatalf("Fail creating window: %v", err)
	}

	// set the title
	Window.SetTitle("CHIP-8")

	// desired screen format and access
	format := sdl.PIXELFORMAT_RGB888
	access := sdl.TEXTUREACCESS_TARGET

	// create a render target for the display
	Screen, err = Renderer.CreateTexture(uint32(format), access, width, height)
	if err != nil {
		log.Fatalf("Fail creating render: %v", err)
	}
}

// clear the renderer, redraw everything, and present.
func redraw() {
	updateScreen()

	Renderer.Clear()

	// draw the screen, log, instructions, and registers
	drawScreen()

	// show it
	Renderer.Present()
}

// copyScreen to the render target at a given location.
func drawScreen() {

	// source area of the screen target
	src := sdl.Rect{
		W: chip8.WIDTH,
		H: chip8.HEIGHT,
	}

	// stretch the render target to fit
	Renderer.Copy(Screen, &src, &sdl.Rect{X: 0, Y: 0, W: width, H: height})
}

// updateScreen with the CHIP-8 video memory.
func updateScreen() {
	if err := Renderer.SetRenderTarget(Screen); err != nil {
		panic(err)
	}

	// the background color for the screen
	Renderer.SetDrawColor(0, 0, 0, 0)
	Renderer.Clear()

	// set the pixel color
	Renderer.SetDrawColor(255, 255, 255, 255)

	for y := int32(0); y < 32; y++ {
		for x := int32(0); x < 64; x++ {
			if VM.Display[(y*64)+x] {
				Renderer.DrawPoint(x, y)
			}
		}
	}

	// restore the render target
	Renderer.SetRenderTarget(nil)
}
