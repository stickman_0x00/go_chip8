package main

import (
	"log"
	"time"

	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/stickman_0x00/go_chip8/chip8"
	debug_window "gitlab.com/stickman_0x00/go_chip8/debug"
)

const (
	rom_path = "roms/games/Tetris.ch8"
)

var (
	VM *chip8.Chip8

	Debug  *debug_window.Debug
	Paused = false
)

func main() {
	var err error

	if err := sdl.Init(sdl.INIT_VIDEO | sdl.INIT_AUDIO); err != nil {
		log.Fatalf("Fail initialize video and audio: %v", err)
	}

	if VM, err = chip8.New(rom_path); err != nil {
		log.Fatalf("Fail loading rom: %v", err)
	}

	dw := debug_window.New(VM)
	create_window()
	initAudio()

	clock := time.NewTicker(time.Millisecond)
	video := time.NewTicker(time.Second / 60)
	sound := time.NewTicker(time.Second / 60)

	for processEvents() {

		select {
		case <-sound.C:
			updateSound()
		case <-video.C:
			redraw()
		case <-clock.C:
			if !Paused {
				VM.Step()
			}
		}
		dw.Redraw()
	}

}

// processEvents from SDL and map keys to the CHIP-8 VM.
func processEvents() bool {
	for e := sdl.PollEvent(); e != nil; e = sdl.PollEvent() {
		switch ev := e.(type) {
		case *sdl.QuitEvent:
			return false
		case *sdl.KeyboardEvent:
			if ev.Type == sdl.KEYUP {
				if key, ok := KeyMap[ev.Keysym.Scancode]; ev.Type == sdl.KEYUP && ok {
					Release_key(key)
				}
			} else {
				if key, ok := KeyMap[ev.Keysym.Scancode]; ok {
					Press_key(key)
				} else {
					switch ev.Keysym.Scancode {
					case sdl.SCANCODE_F5:
						if Paused {
							VM.Step()
						}
					case sdl.SCANCODE_F10:
						Paused = !Paused
					}
				}
			}
		}
	}

	return true
}
