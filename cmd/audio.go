package main

import (
	"encoding/binary"
	"math"
	"time"

	"github.com/veandco/go-sdl2/sdl"
)

var (
	// AudioDevice is the audio device for emitting sounds.
	AudioDevice sdl.AudioDeviceID

	// ObtainedSpec is the spec opened for the device.
	ObtainedSpec *sdl.AudioSpec
)

// initAudio initializes an audio device for the CHIP-8 virtual machine.
func initAudio() {
	var err error

	// the desired audio specification
	desiredSpec := &sdl.AudioSpec{
		Freq:     64 * 60,
		Format:   sdl.AUDIO_F32LSB,
		Channels: 1,
		Samples:  64,
	}

	ObtainedSpec = &sdl.AudioSpec{}

	// open the device and start playing it
	if sdl.GetNumAudioDevices(false) > 0 {
		if AudioDevice, err = sdl.OpenAudioDevice("", false, desiredSpec, ObtainedSpec, sdl.AUDIO_ALLOW_ANY_CHANGE); err != nil {
			panic(err)
		}

		sdl.PauseAudioDevice(AudioDevice, false)
	}
}

func updateSound() {
	if AudioDevice != 0 {
		sample := make([]byte, 4)

		// set the sample sample bytes
		if time.Now().UnixNano() < VM.ST {
			binary.LittleEndian.PutUint32(sample, math.Float32bits(1.0))
		}

		// N channels, each channel has S samples (4 bytes each)
		n := int(ObtainedSpec.Channels) * int(ObtainedSpec.Samples) * 4
		data := make([]byte, n)

		// 128 samples per 1/60 of a second
		for i := 0; i < n; i += 4 {
			copy(data[i:], sample)
		}

		if err := sdl.QueueAudio(AudioDevice, data); err != nil {
			println(err)
		}
	}
}
