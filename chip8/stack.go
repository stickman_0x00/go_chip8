package chip8

import "log"

const stack_size = 16

type Stack struct {
	stack   [stack_size]uint16
	pointer uint
}

func (me *Stack) Pop() uint16 {
	if me.pointer == 0 {
		log.Fatal("Stack underflow")
	}

	me.pointer--

	return me.stack[me.pointer]
}

func (me *Stack) Push(b uint16) {
	if me.pointer == stack_size {
		log.Fatal("Stack overflow")
	}

	me.stack[me.pointer] = b
	me.pointer++
}
