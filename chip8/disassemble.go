package chip8

import (
	"fmt"
)

type disassemble_handler func(inst uint16, c *Chip8) string

var disassemble_handlers = map[uint16]disassemble_handler{
	0x0000: disassemble_0x0000,
	0x1000: disassemble_0x1000,
	0x2000: disassemble_0x2000,
	0x3000: disassemble_0x3000,
	0x4000: disassemble_0x4000,
	0x5000: disassemble_0x5000,
	0x6000: disassemble_0x6000,
	0x7000: disassemble_0x7000,
	0x8000: disassemble_0x8000,
	0x9000: disassemble_0x9000,
	0xA000: disassemble_0xA000,
	0xB000: disassemble_0xB000,
	0xC000: disassemble_0xC000,
	0xD000: disassemble_0xD000,
	0xE000: disassemble_0xE000,
	0xF000: disassemble_0xF000,
}

func disassemble_0x0000(inst uint16, c *Chip8) string {
	_, _, _, _, kk := c.Decode(inst)

	if kk == 0xE0 {
		return "CLS"
	} else if kk == 0xEE {
		return "RET"
	}

	return ""
}

func disassemble_0x1000(inst uint16, c *Chip8) string {
	addr, _, _, _, _ := c.Decode(inst)

	return fmt.Sprintf("JP    0x%04X", addr)
}

func disassemble_0x2000(inst uint16, c *Chip8) string {
	addr, _, _, _, _ := c.Decode(inst)

	return fmt.Sprintf("CALL  0x%04X", addr)
}

func disassemble_0x3000(inst uint16, c *Chip8) string {
	_, _, x, _, kk := c.Decode(inst)

	return fmt.Sprintf("SE    V%d, 0x%02X", x, kk)
}

func disassemble_0x4000(inst uint16, c *Chip8) string {
	_, _, x, _, kk := c.Decode(inst)

	return fmt.Sprintf("SNE   V%d, 0x%02X", x, kk)
}

func disassemble_0x5000(inst uint16, c *Chip8) string {
	_, _, x, y, _ := c.Decode(inst)

	return fmt.Sprintf("SE    V%d, V%d", x, y)
}

func disassemble_0x6000(inst uint16, c *Chip8) string {
	_, _, x, _, kk := c.Decode(inst)

	return fmt.Sprintf("LD    V%d, 0x%02X", x, kk)
}

func disassemble_0x7000(inst uint16, c *Chip8) string {
	_, _, x, _, kk := c.Decode(inst)

	return fmt.Sprintf("ADD   V%d, 0x%02X", x, kk)
}

func disassemble_0x8000(inst uint16, c *Chip8) string {
	_, n, x, y, _ := c.Decode(inst)

	switch n {
	case 0:
		return fmt.Sprintf("LD    V%d, V%d", x, y)
	case 1:
		return fmt.Sprintf("OR    V%d, V%d", x, y)
	case 2:
		return fmt.Sprintf("AND   V%d, V%d", x, y)
	case 3:
		return fmt.Sprintf("XOR   V%d, V%d", x, y)
	case 4:
		return fmt.Sprintf("ADD   V%d, V%d", x, y)
	case 5:
		return fmt.Sprintf("SUB   V%d, V%d", x, y)
	case 6:
		return fmt.Sprintf("SHR   V%d, V%d", x, y)
	case 7:
		return fmt.Sprintf("SUBN  V%d, V%d", x, y)
	case 0xE:
		return fmt.Sprintf("SHL   V%d{, V%d}", x, y)
	}

	return ""
}

func disassemble_0x9000(inst uint16, c *Chip8) string {
	_, _, x, y, _ := c.Decode(inst)

	return fmt.Sprintf("SNE   V%d, V%d", x, y)
}

func disassemble_0xA000(inst uint16, c *Chip8) string {
	addr, _, _, _, _ := c.Decode(inst)

	return fmt.Sprintf("LD    I, 0x%04X", addr)
}

func disassemble_0xB000(inst uint16, c *Chip8) string {
	addr, _, _, _, _ := c.Decode(inst)

	return fmt.Sprintf("JP    V0, 0x%04X", addr)
}

func disassemble_0xC000(inst uint16, c *Chip8) string {
	_, _, x, _, kk := c.Decode(inst)

	return fmt.Sprintf("RND   V%d, 0x%2X", x, kk)
}

func disassemble_0xD000(inst uint16, c *Chip8) string {
	_, n, x, y, _ := c.Decode(inst)

	return fmt.Sprintf("DRW   V%d, V%d, 0x%x", x, y, n)
}

func disassemble_0xE000(inst uint16, c *Chip8) string {
	_, _, x, _, kk := c.Decode(inst)

	switch kk {
	case 0x9E:
		return fmt.Sprintf("SKP   V%d", x)
	case 0xA1:
		return fmt.Sprintf("SKNP  V%d", x)
	}

	return ""
}

func disassemble_0xF000(inst uint16, c *Chip8) string {
	_, _, x, _, kk := c.Decode(inst)

	switch kk {
	case 0x07:
		return fmt.Sprintf("LD    V%d, DT", x)
	case 0x0A:
		return fmt.Sprintf("LD    V%d, K", x)
	case 0x15:
		return fmt.Sprintf("LD    DT, V%d", x)
	case 0x18:
		return fmt.Sprintf("LD    ST, V%d", x)
	case 0x1E:
		return fmt.Sprintf("ADD   I, V%d", x)
	case 0x29:
		return fmt.Sprintf("LD    F, V%d", x)
	case 0x33:
		return fmt.Sprintf("LD    B, V%d", x)
	case 0x55:
		return fmt.Sprintf("LD    [I], V%d", x)
	case 0x65:
		return fmt.Sprintf("LD    V%d, [I]", x)
	}

	return ""
}

// Disassemble a CHIP-8 instruction.
func (me *Chip8) Disassemble(inst uint16) string {
	var s string
	if f, exist := disassemble_handlers[inst&0xF000]; exist {
		s = f(inst, me)
	}

	if s == "" {
		return "??"
	}

	return s
}
