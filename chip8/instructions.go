package chip8

import (
	"math/rand"
	"time"
)

type instruction_handler func(inst uint16, c *Chip8)

var instruction_handlers = map[uint16]instruction_handler{
	0x0000: instruction_0x0000,
	0x1000: instruction_0x1000,
	0x2000: instruction_0x2000,
	0x3000: instruction_0x3000,
	0x4000: instruction_0x4000,
	0x5000: instruction_0x5000,
	0x6000: instruction_0x6000,
	0x7000: instruction_0x7000,
	0x8000: instruction_0x8000,
	0x9000: instruction_0x9000,
	0xA000: instruction_0xA000,
	0xB000: instruction_0xB000,
	0xC000: instruction_0xC000,
	0xD000: instruction_0xD000,
	0xE000: instruction_0xE000,
	0xF000: instruction_0xF000,
}

func instruction_0x0000(inst uint16, c *Chip8) {
	_, _, _, _, kk := c.Decode(inst)

	if kk == 0xE0 {
		c.Display = [WIDTH * HEIGHT]bool{}
	} else if kk == 0xEE {
		c.PC = c.Stack.Pop()
	}

}

func instruction_0x1000(inst uint16, c *Chip8) {
	c.PC, _, _, _, _ = c.Decode(inst)
}

func instruction_0x2000(inst uint16, c *Chip8) {
	c.Stack.Push(c.PC)

	c.PC, _, _, _, _ = c.Decode(inst)
}

func instruction_0x3000(inst uint16, c *Chip8) {
	_, _, x, _, kk := c.Decode(inst)

	if kk == c.V[x] {
		c.PC += 2
	}
}

func instruction_0x4000(inst uint16, c *Chip8) {
	_, _, x, _, kk := c.Decode(inst)

	if kk != c.V[x] {
		c.PC += 2
	}
}

func instruction_0x5000(inst uint16, c *Chip8) {
	_, _, x, y, _ := c.Decode(inst)

	if c.V[x] == c.V[y] {
		c.PC += 2
	}
}

func instruction_0x6000(inst uint16, c *Chip8) {
	_, _, x, _, kk := c.Decode(inst)

	c.V[x] = kk
}

func instruction_0x7000(inst uint16, c *Chip8) {
	_, _, x, _, kk := c.Decode(inst)

	c.V[x] += kk
}

func instruction_0x8000(inst uint16, c *Chip8) {
	_, n, x, y, _ := c.Decode(inst)

	switch n {
	case 0:
		c.V[x] = c.V[y]
	case 1:
		c.V[x] |= c.V[y]
	case 2:
		c.V[x] &= c.V[y]
	case 3:
		c.V[x] ^= c.V[y]
	case 4:
		c.V[0xF] = 0
		if c.V[x]+c.V[y] > 255 {
			c.V[0xF] = 1
		}

		c.V[x] += c.V[y]
	case 5:
		c.V[0xF] = 0
		if c.V[x] > c.V[y] {
			c.V[0xF] = 1
		}

		c.V[x] -= c.V[y]
	case 6:
		c.V[0xF] = c.V[x] & 0b1

		c.V[x] /= 2
	case 7:
		if c.V[y] > c.V[x] {
			c.V[0xF] = 1
		}

		c.V[x] = c.V[y] - c.V[x]
	case 0xE:
		c.V[0xF] = (c.V[x] & (1 << 7) >> 7)

		c.V[x] *= 2
	}
}

func instruction_0x9000(inst uint16, c *Chip8) {
	_, _, x, y, _ := c.Decode(inst)

	if c.V[x] != c.V[y] {
		c.PC += 2
	}
}

func instruction_0xA000(inst uint16, c *Chip8) {
	c.I, _, _, _, _ = c.Decode(inst)
}

func instruction_0xB000(inst uint16, c *Chip8) {
	addr, _, _, _, _ := c.Decode(inst)

	c.PC = addr + uint16(c.V[0])
}

func instruction_0xC000(inst uint16, c *Chip8) {
	_, _, x, _, kk := c.Decode(inst)

	c.V[x] = byte(rand.Intn(256)) & kk
}

func instruction_0xD000(inst uint16, c *Chip8) {
	_, n, x, y, _ := c.Decode(inst)

	c.V[0xF] = 0

	/*
		Sprite looks example
		11110000
		10010000
		10010000
		10010000
		11110000
	*/
	// Row by row
	for row := uint16(0); row < uint16(n); row++ {
		// Grabbing 8-bits of memory, or a single row of a sprite
		// The technical reference states we start at the address stored in I
		sprite := c.ROM[c.I+row]

		// We have a width variable set to 8 because each sprite is 8 pixels wide
		for col := uint16(0); col < 8; col++ {
			index := (uint16(c.V[y])+row)*64 + (uint16(c.V[x]) + col)
			if index > uint16(len(c.Display)) {
				continue
			}
			if (sprite & (0x80 >> col)) != 0 {
				if c.Display[index] == true {
					c.V[0xF] = 1
				}

				if c.Display[index] {
					c.Display[index] = false
				} else {
					c.Display[index] = true
				}
			}
		}
	}
}

func instruction_0xE000(inst uint16, c *Chip8) {
	_, _, x, _, kk := c.Decode(inst)

	switch kk {
	case 0x9E:
		if c.Keyboard[c.V[x]] {
			c.PC += 2
		}
	case 0xA1:
		if !c.Keyboard[c.V[x]] {
			c.PC += 2
		}
	}
}

func instruction_0xF000(inst uint16, c *Chip8) {
	_, _, x, _, kk := c.Decode(inst)

	switch kk {
	case 0x07:
		// c.V[x] = c.DT
		c.V[x] = c.GetDelayTimer()
	case 0x0A:
		// TODO: Check this later
		for i, k := range c.Keyboard {
			if k {
				c.V[x] = byte(i)
				break
			}
		}
	case 0x15:
		// c.DT = c.V[x]
		c.DT = time.Now().UnixNano() + int64(c.V[x])*1000000000/60
	case 0x18:
		// c.ST = c.V[x]
		c.ST = time.Now().UnixNano() + int64(c.V[x])*1000000000/60
	case 0x1E:
		c.I += uint16(c.V[x])
	case 0x29:
		// Multiply by 5 since every sprite is 5 bytes
		c.I = uint16(c.V[x]*5 + FONT_ADDRESS)
	case 0x33:
		c.ROM[c.I] = c.V[x] / 100
		c.ROM[c.I+1] = c.V[x] / 10 % 10
		c.ROM[c.I+2] = c.V[x] % 10
	case 0x55:
		for i := byte(0); i <= x; i++ {
			c.ROM[c.I+uint16(i)] = c.V[i]
		}
	case 0x65:
		for i := byte(0); i <= x; i++ {
			c.V[i] = c.ROM[c.I+uint16(i)]
		}
	}
}
