package chip8

import (
	"encoding/binary"
	"log"
	"os"
	"time"
)

const (
	BASE_ADDRESS = 0x200
	// x
	WIDTH = 64
	// y
	HEIGHT = 32
)

type Chip8 struct {
	// Memory:
	// The Chip-8 language is capable of accessing up to 4KB (4,096 bytes) of RAM, from location 0x000 (0) to 0xFFF (4095).
	// 0x1000 == 4096
	// Memory Map:
	// +---------------+= 0xFFF (4095) End of Chip-8 RAM
	// |               |
	// |               |
	// |               |
	// |               |
	// |               |
	// | 0x200 to 0xFFF|
	// |     Chip-8    |
	// | Program / Data|
	// |     Space     |
	// |               |
	// |               |
	// |               |
	// +- - - - - - - -+= 0x600 (1536) Start of ETI 660 Chip-8 programs
	// |               |
	// |               |
	// |               |
	// +---------------+= 0x200 (512) Start of most Chip-8 programs
	// | 0x000 to 0x1FF|
	// | Reserved for  |
	// |  interpreter  |
	// +---------------+= 0x000 (0) Start of Chip-8 RAM
	ROM [0x1000]byte

	// Registers:
	// Chip-8 has 16 general purpose 8-bit registers
	V [16]byte
	// This register is generally used to store memory addresses
	I uint16
	// Delay Timer
	DT int64
	// Sound Timer
	ST int64
	// Program counter
	PC uint16
	// Stack
	Stack

	// Keyboard
	// if true key is pressed
	Keyboard [16]bool

	// Display
	// Chip-8 language used a 64x32-pixel
	// 32 * 64 = 2048 = 0x800
	Display [WIDTH * HEIGHT]bool
}

func New(file string) (*Chip8, error) {
	c := &Chip8{}

	// load rom
	rom, err := os.ReadFile(file)
	if err != nil {
		return nil, err
	}
	copy(c.ROM[BASE_ADDRESS:], rom[:])

	c.load_font()

	c.PC = BASE_ADDRESS

	return c, nil
}

func (me *Chip8) Get_instruction(address uint16) uint16 {
	if int(address) == len(me.ROM) {
		log.Fatalf("Address invalid: 0x%04X", address)
	}

	return binary.BigEndian.Uint16(me.ROM[address : address+2])
}

// Receive memory address to decode
// Return:
// nnn or addr - A 12-bit value, the lowest 12 bits of the instruction
// n or nibble - A 4-bit value, the lowest 4 bits of the instruction
// x - A 4-bit value, the lower 4 bits of the high byte of the instruction
// y - A 4-bit value, the upper 4 bits of the low byte of the instruction
// kk or byte - An 8-bit value, the lowest 8 bits of the instruction
func (me *Chip8) Decode(instruction uint16) (addr uint16, n, x, y, kk byte) {
	addr = instruction & 0x0FFF
	n = byte(instruction & 0x000F)

	x = byte(instruction & 0x0F00 >> 8)
	y = byte(instruction & 0x00F0 >> 4)

	kk = byte(instruction & 0x00FF)

	return addr, n, x, y, kk
}

func (me *Chip8) Step() {
	inst := me.Get_instruction(me.PC)
	me.PC += 2

	if f, exist := instruction_handlers[inst&0xF000]; exist {
		f(inst, me)
	}
}

// Converts a CHIP-8 delay timer register to a byte.
func (me *Chip8) GetDelayTimer() byte {
	now := time.Now().UnixNano()

	if now < me.DT {
		return uint8((me.DT - now) * 60 / 1000000000)
	}

	return 0
}

// Converts the CHIP-8 sound timer register to a byte.
func (vm *Chip8) GetSoundTimer() byte {
	now := time.Now().UnixNano()

	if now < vm.ST {
		return uint8((vm.ST - now) * 60 / 1000000000)
	}

	return 0
}
