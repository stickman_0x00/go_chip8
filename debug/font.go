package debug_window

import (
	"log"

	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/ttf"
)

const (
	font_path = "debug/assets/Go-Mono-Bold.ttf"
	font_size = 18
)

var (
	font_height int32 = 0
)

// loadFont loads the bitmap surface with font on it.
func (me *Debug) load_font() {
	var err error

	if err = ttf.Init(); err != nil {
		log.Fatalf("Fail loading font: %v", err)
	}

	if me.font, err = ttf.OpenFont(font_path, font_size); err != nil {
		log.Fatalf("Fail loading font: %v", err)
	}

	// get font height
	surface, _ := me.font.RenderUTF8Shaded("A", sdl.Color{R: 255, G: 255, B: 255, A: 255}, sdl.Color{R: 0, G: 0, B: 0, A: 0})
	texture, _ := me.renderer.CreateTextureFromSurface(surface)
	_, _, _, font_height, _ = texture.Query()
	surface.Free()
	texture.Destroy()
}
