package debug_window

import (
	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/ttf"
	"gitlab.com/stickman_0x00/go_chip8/chip8"
)

type Debug struct {
	window   *sdl.Window
	renderer *sdl.Renderer
	surface  *sdl.Surface
	font     *ttf.Font
	// font *sdl.Texture

	VM *chip8.Chip8
}

func New(VM *chip8.Chip8) *Debug {
	d := &Debug{
		VM: VM,
	}

	d.create_window()
	d.load_font()

	return d
}
