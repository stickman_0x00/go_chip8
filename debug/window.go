package debug_window

import (
	"fmt"
	"log"
	"strconv"

	"github.com/veandco/go-sdl2/sdl"
)

const (
	width  = 524
	height = 500
	frame  = 10
	// number of rows showed in table
	memory_show = 18
)

func (me *Debug) create_window() {
	var err error

	// window attributes
	flags := sdl.WINDOW_OPENGL

	dm, err := sdl.GetCurrentDisplayMode(0)
	if err != nil {
		log.Fatalf("Fail getting current display: %v", err)
	}

	me.window, err = sdl.CreateWindow("Debug", dm.W-width, dm.H-height, width, height, uint32(flags))
	if err != nil {
		log.Fatalf("Fail creating window: %v", err)
	}

	me.renderer, err = sdl.CreateRenderer(me.window, 0, 0)
	if err != nil {
		log.Fatalf("Fail creating renderer: %v", err)
	}
}

func (me *Debug) Redraw() {
	// clear the renderer
	me.renderer.SetDrawColor(32, 42, 53, 255)
	me.renderer.Clear()
	me.window.UpdateSurface()

	me.draw_frame()

	me.draw_registers()

	me.draw_table_instructions()

	// show it
	me.renderer.Present()
}

func (me *Debug) draw_frame() {
	me.renderer.SetDrawColor(255, 255, 255, 255)
	// top bar
	for y := int32(0); y < frame; y++ {
		me.renderer.DrawLine(0, y, width, y)
	}

	// left bar
	for x := int32(0); x < frame; x++ {
		me.renderer.DrawLine(x, 0, x, height)
	}

	// bottom bar
	for y := int32(0); y < frame; y++ {
		me.renderer.DrawLine(0, height-y, width, height-y)
	}

	// right bar
	for x := int32(0); x < frame; x++ {
		me.renderer.DrawLine(width-x, 0, width-x, height)
	}
}

func (me *Debug) draw_registers() {
	for i := 0; i < 4; i++ {
		var s string

		for j := 0; j < 4; j++ {
			s += fmt.Sprintf("%3s = x%02X  ", "V"+strconv.Itoa((i*4)+(j)), me.VM.V[i])
		}

		surface, _ := me.font.RenderUTF8Blended(s, sdl.Color{R: 255, G: 255, B: 255, A: 255})
		texture, _ := me.renderer.CreateTextureFromSurface(surface)
		_, _, w, _, _ := texture.Query()
		// Center text
		X := width - w - frame
		me.renderer.Copy(texture, nil, &sdl.Rect{X: X, Y: (frame + 5) + (int32(i) * font_height), W: w, H: font_height})
		surface.Free()
		texture.Destroy()
	}
}

func (me *Debug) draw_table_instructions() {
	var X_bar_begin int32 = frame * 2
	var X_bar_end int32 = width - X_bar_begin

	// left bar
	me.renderer.DrawLine(X_bar_begin, font_height*5, X_bar_begin, font_height*(5+memory_show)-1)
	// right bar
	me.renderer.DrawLine(X_bar_end, font_height*5, X_bar_end, font_height*(5+memory_show)-1)

	for i := int32(0); i < memory_show; i++ {
		var Y int32 = font_height * (5 + i) // 5 because 4 lines of registers + the actual one
		// horizontal bar
		me.renderer.DrawLine(X_bar_begin, Y-1, X_bar_end, Y-1)

		inst := me.VM.Get_instruction(me.VM.PC + uint16(i*2))

		s := fmt.Sprintf("0x%03X | 0x%04X | %-26s", me.VM.PC+uint16(2*i), inst, me.VM.Disassemble(inst))
		surface, _ := me.font.RenderUTF8Blended(s, sdl.Color{R: 255, G: 255, B: 255, A: 255})
		texture, _ := me.renderer.CreateTextureFromSurface(surface)
		_, _, w, _, _ := texture.Query()

		// me.renderer.DrawLine(X_bar_begin, Y_bar, X_bar_end, Y_bar)

		me.renderer.Copy(texture, nil, &sdl.Rect{X: X_bar_begin + 5, Y: Y, W: w, H: font_height})

		surface.Free()
		texture.Destroy()
	}

	me.renderer.DrawLine(X_bar_begin, font_height*(5+memory_show)-1, X_bar_end, font_height*(5+memory_show)-1)
}
