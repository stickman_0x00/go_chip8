# CHIP-8

# RUN

```bash
go run cmd/*.go
```

# Resources

- http://devernay.free.fr/hacks/chip8/C8TECH10.HTM
- https://github.com/trapexit/chip-8_documentation
- https://en.wikipedia.org/wiki/CHIP-8
- https://austinmorlan.com/posts/chip8_emulator/
- https://medium.com/average-coder/exploring-emulation-in-go-chip-8-636f99683f2a
- https://www.freecodecamp.org/news/creating-your-very-own-chip-8-emulator/
- https://www.arjunnair.in/p37/

## Source code

- https://github.com/giawa/chip8
- https://github.com/jamesmcm/chip8go
- https://github.com/theothertomelliott/chip8
- https://github.com/bomer/chip8
- https://github.com/massung/CHIP-8

## Assets
- https://soundbible.com/838-Censor-Beep.html